export function fight(firstFighter, secondFighter) {
  let f1 = Object.assign({}, firstFighter);
  let f2 = Object.assign({}, secondFighter);

  while (true) {
    if (!turn(f1, f2)) {
      return f1;
    }
    if (!turn(f2, f1)) {
      return f2;
    }
  }
}

function turn(attacker, defender) {
  defender.health -= getDamage(attacker, defender);
  if (defender.health <= 0) {
    return false;
  }
  return defender.health;
}

export function getDamage(attacker, enemy) {
  return getHitPower(attacker) - getBlockPower(enemy);
}

export function getHitPower(fighter) {
  const { attack } = fighter;
  const criticalHitChance = Math.random() + 1;
  return attack * criticalHitChance;
}

export function getBlockPower(fighter) {
  const { defense } = fighter;
  const dodgeChance = Math.random() + 1;
  return defense * dodgeChance;
}