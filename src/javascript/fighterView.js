import { createElement } from './helpers/domHelper';
import { createImage } from './helpers/domHelper';

export function createFighter(fighter, handleClick, selectFighter) {
  const { name, source } = fighter;
  const nameElement = createName(name);
  const imageElement = createImage(source, 'fighter-image');
  const checkboxElement = createCheckbox(source);
  const fighterContainer = createElement({ tagName: 'div', className: 'fighter' });

  fighterContainer.append(imageElement, nameElement, checkboxElement);

  const preventCheckboxClick = (ev) => ev.stopPropagation();
  const onCheckboxClick = (ev) => selectFighter(ev, fighter);
  const onCheckboxClick2 = (ev) => switchSelected(ev, fighterContainer);
  const onFighterClick = (ev) => handleClick(ev, fighter);

  fighterContainer.addEventListener('click', onFighterClick, false);
  checkboxElement.addEventListener('change', onCheckboxClick, false);
  checkboxElement.addEventListener('change', onCheckboxClick2, false);
  checkboxElement.addEventListener('click', preventCheckboxClick, false);

  return fighterContainer;
}

function switchSelected(event, fighterContainer) {
  if (event.target.checked) {
    fighterContainer.style.backgroundColor = 'rgba(168, 0, 240, 0.44)';
    fighterContainer.style.border = '1px solid black';
  } else {
    fighterContainer.style.backgroundColor = '';
    fighterContainer.style.borderColor = 'transparent';

  }
}

function createName(name) {
  const nameElement = createElement({ tagName: 'span', className: 'name' });
  nameElement.innerText = name;

  return nameElement;
}

function createCheckbox() {
  const label = createElement({ tagName: 'label', className: 'custom-checkbox' });
  const span = createElement({ tagName: 'span', className: 'checkmark' });
  const attributes = { type: 'checkbox' };
  const checkboxElement = createElement({ tagName: 'input', attributes });

  label.append(checkboxElement, span);
  return label;
}