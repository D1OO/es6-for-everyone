import { createElement } from '../helpers/domHelper';
import { createImage } from '../helpers/domHelper';
import { showModal } from './modal';

export function showFighterDetailsModal(fighter) {
  const title = 'Fighter info';
  const bodyElement = createFighterDetails(fighter);
  showModal({ title, bodyElement });
}

function createFighterDetails(fighter) {
  const { name, attack, defense, health, source: fighterImage } = fighter;

  const fighterDetails = createElement({ tagName: 'div', className: 'modal-body' });
  const nameElement = createElement({ tagName: 'p', className: 'fighter-name' });
  const statsContainer = createElement({ tagName: 'div', className: 'stats-container' });
  const imageContainer = createElement({ tagName: 'div', className: 'image-container' });

  imageContainer.append(createImage(fighterImage, 'fighter-image'));
  nameElement.innerText = name;
  statsContainer.append(createStatBlock('HP', '🧡 ' + health));
  statsContainer.append(createStatBlock('ATT', '🔪 ' + attack));
  statsContainer.append(createStatBlock('DEF', '🛡️ ' + defense));

  fighterDetails.append(nameElement);
  fighterDetails.append(imageContainer);
  fighterDetails.append(statsContainer);
  return fighterDetails;
}

function createStatBlock(name, value) {
  const statBlock = createElement({ tagName: 'div', className: 'stat-block' });
  const statValue = createElement({ tagName: 'span', className: 'stat-value' });
  const statLabel = createElement({ tagName: 'span', className: 'stat-name' });
  statBlock.append(statValue);
  statBlock.append(statLabel);

  statLabel.innerText = name;
  statValue.innerText = value;

  return statBlock;
}
