import { createElement } from '../helpers/domHelper';
import { createImage } from '../helpers/domHelper';
import { showModal } from './modal';

export function showWinnerModal(fighter) {
  const title = 'The Winner is...';
  const bodyElement = createWinnerDetails(fighter);
  showModal({ title, bodyElement, type: 'winner-modal' });
}

function createWinnerDetails(fighter) {
  const { name, source: image } = fighter;

  const fighterDetails = createElement({ tagName: 'div', className: 'modal-body' });
  const nameElement = createElement({ tagName: 'p', className: 'fighter-name' });
  const imageContainer = createElement({ tagName: 'div', className: 'image-container' });

  imageContainer.append(createImage(image, 'winner-image'));
  nameElement.innerText = name + '!!!';
  nameElement.style.color = 'red';
  nameElement.style.fontWeight = '900';

  fighterDetails.append(nameElement);
  fighterDetails.append(imageContainer);

  return fighterDetails;
}